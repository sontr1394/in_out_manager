import axios from "axios";
import { BASE_URL } from "./configURL";

export const userService = {
  postLogin: (dataLogin) => {
    return axios({
      url: `${BASE_URL}/user`,
      method: "POST",
      data: dataLogin,
    });
  },
};
