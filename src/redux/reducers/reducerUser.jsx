import { localServ } from "../../services/localService";
import { SET_USER } from "../constant/constantUser";
let initialState = {
  userInfo: localServ.user.get(),
};

export const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_USER:
      console.log("action.payload", action.payload);
      state.userInfo = action.payload.data;
      return { ...state };

    default:
      return state;
  }
};
