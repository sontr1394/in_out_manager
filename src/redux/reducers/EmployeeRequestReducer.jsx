import moment from "moment";
import {
  ADD_NEW_REQUEST,
  DELETE_REQUEST,
  EDIT_REQUEST,
  UPDATE_REQUEST,
} from "../constant/personconst";

const initialState = {
  requestList: [
    {
      requestID: 1,
      employeeID: 3143,
      requestType: "Vào muộn",
      requestReason: "Xin nghỉ phép nửa ca",
      requestDate: "2022-10-03",
      requestTime: "11:45",
      requestStatus: "Mới",
    },
    {
      requestID: 2,
      employeeID: 3145,
      requestType: "Về sớm ",
      requestReason: "Xin nghỉ phép nửa ca",
      requestDate: "2022-10-03",
      requestTime: "12:05",
      requestStatus: "Mới",
    },
  ],
  requestEdit: {
    requestID: "",
    employeeID: "",
    requestType: "Đi muộn",
    requestReason: "",
    requestDate: moment().format("YYYY-MM-DD"),
    requestTime: moment().format("HH:MM"),
    requestStatus: "Mới",
  },
};

export const EmployeeRequestReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_NEW_REQUEST: {
      const requestList = state.requestList;
      // console.log("requestList ", requestList);
      let length = requestList.length;
      // console.log("length: ", length);
      if (length === 0) {
        action.newRequest.requestID = 1;
      } else {
        let requestadd = { ...action.newRequest };
        requestadd.requestID = requestList[length - 1].requestID + 1;
        // console.log("new id", requestadd);
        action.newRequest = requestadd;
      }
      const requestListUpdate = [...requestList, action.newRequest];
      state.requestList = requestListUpdate;
      // console.log("state: ", state);

      return { ...state };
    }

    case EDIT_REQUEST: {
      state.requestEdit = action.request;
      // console.log("edit select to change", state.requestEdit);
      return { ...state };
    }

    case UPDATE_REQUEST: {
      // let request = action.request;
      state.requestEdit = action.request;
      let requestListUpdate = [...state.requestList];
      let index = requestListUpdate.findIndex((item) => {
        return item.requestID === state.requestEdit.requestID;
      });
      if (index !== -1) {
        requestListUpdate[index] = state.requestEdit;
        state.requestList = requestListUpdate;
        state.requestEdit = {
          requestID: "",
          employeeID: "",
          requestType: "Đi muộn",
          requestReason: "",
          requestDate: moment().format("YYYY-MM-DD"),
          requestTime: moment().format("HH:MM"),
          requestStatus: "Mới",
        };
      }
      // console.log("state after update", state);
      return { ...state };
    }

    case DELETE_REQUEST: {
      let requestListUpdate = [...state.requestList];
      requestListUpdate = requestListUpdate.filter((request) => {
        return request.requestID !== action.request.requestID;
      });
      state.requestList = requestListUpdate;
      return { ...state };
    }
    default:
      return { ...state };
  }
};
