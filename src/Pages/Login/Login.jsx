import React from "react";
import { Button, Col, Form, Input, message, Row } from "antd";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { userService } from "../../services/userServices";
import { SET_USER } from "../../redux/constant/constantUser";
import { localServ } from "../../services/localService";

export default function Login() {
  let navigate = useNavigate();

  let dispatch = useDispatch();

  const onFinish = (values) => {
    console.log("Success:", values);
    userService.postLogin(values).then((res) => {
      console.log(res);
      //luu vao local storage
      localServ.user.set(res.data);

      dispatch({
        type: SET_USER,
        payload: res,
      });
      message.success("Đăng nhập thành công");
      setTimeout(() => {
        navigate("/request");
      }, 2000);
    });
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <div className="grid h-screen place-items-center">
      <Row type="flex" align="middle">
        <Col span={12}>
          <img src="./img/ManiQoute.jpg" alt="Mani Logo" />
        </Col>
        <Col span={12} align="center">
          <Form
            className="w-full ml-5"
            layout="vertical"
            name="basic"
            labelCol={{
              span: 8,
            }}
            wrapperCol={{
              span: 24,
            }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
          >
            <img src="./img/logo.png" className="bg" alt="Mani logo" />
            <Form.Item
              label="Username"
              name="taiKhoan"
              rules={[
                {
                  required: true,
                  message: "Please input your username!",
                },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              label="Password"
              name="matKhau"
              rules={[
                {
                  required: true,
                  message: "Please input your password!",
                },
              ]}
            >
              <Input.Password />
            </Form.Item>

            <Form.Item
              wrapperCol={{
                offset: 8,
                span: 16,
              }}
            >
              <Button type="primary" htmlType="submit">
                Submit
              </Button>
            </Form.Item>
          </Form>
        </Col>
      </Row>
    </div>
  );
}
