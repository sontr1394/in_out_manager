import React from "react";
import { useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import { localServ } from "../../services/localService";

export default function UserNav() {
  let user = useSelector((state) => {
    // console.log("user info, ", state.userReducer.userInfo.data.userID);
    return state.userReducer.userInfo;
  });

  let handleLogout = () => {
    localServ.user.remove();
    window.location.href = "/login";
  };

  let renderContent = () => {
    if (user) {
      return (
        <div>
          <span className=" font-weight-bold mr-5 py-2 px-3">
            Người dùng: {user.userID}
          </span>
          <button
            className="border rounded px-5 py-2"
            onClick={() => {
              handleLogout();
            }}
          >
            Đăng xuất
          </button>
        </div>
      );
    } else {
      return (
        <>
          <NavLink to="/login">
            <button className="border rounded px-5 py-2 mr-4">Đăng nhập</button>
            <button className="border rounded px-5 py-2">Đăng ký</button>
          </NavLink>
        </>
      );
    }
  };

  return <div>{renderContent()}</div>;
}
