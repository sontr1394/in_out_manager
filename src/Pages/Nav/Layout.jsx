import React from "react";
import Header from "./Header";

export default function Layout({ Component }) {
  return (
    <div>
      <Header />
      <Component />
    </div>
  );
}
