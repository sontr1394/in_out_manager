import { DatePicker, TimePicker } from "antd";
import moment from "moment";
import React, { Component } from "react";
import { connect } from "react-redux";
import Swal from "sweetalert2";
import { ADD_NEW_REQUEST } from "../../../redux/constant/personconst";

class RequestForm extends Component {
  state = {
    requestID: "",
    employeeID: "",
    requestType: "Đi muộn",
    requestReason: "",
    requestDate: moment().format("YYYY-MM-DD"),
    requestTime: "",
    requestStatus: "Mới",
  };

  handleChange = (e) => {
    let tagInput = e.target;
    let { name, value } = tagInput;
    console.log("name, value ", name, value);
    this.setState(
      {
        [name]: value,
      },
      () => {
        console.log(this.state);
      }
    );
  };

  handleDateValue = (date) => {
    this.setState(
      {
        requestDate: moment(date).format("YYYY-MM-DD"),
      },
      () => {
        console.log(this.state);
      }
    );
  };

  disabledDate = (current) => {
    // Can not select days before today and today
    return current && current.valueOf() < Date.now();
  };

  handleTime = (newTime) => {
    var time = newTime.format("HH:mm");
    this.setState(
      {
        requestTime: time,
      },
      () => {
        console.log(this.state);
      }
    );
  };

  checkValid = () => {
    for (let key in this.state) {
      if (key !== "requestID" && this.state[key] === "") {
        return false;
      }
    }
    return true;
  };

  handleCreateNewRequest = (newRequest) => {
    if (this.checkValid()) {
      this.props.addNewRequest(newRequest);
    } else {
      Swal.fire({
        icon: "error",
        title: "Lỗi...",
        text: "Bạn phải nhập đủ tất cả các trường!",
      });
      // message.error("Phải nhập giá trị cho tất cả các trường");
    }
  };

  render() {
    const format = "HH:mm";
    return (
      <div className="container">
        <div className="card">
          <div className="card-header bg-dark text-white h4 text-center">
            Đăng ký yêu cầu
          </div>
          <div className="card-body">
            <form>
              <div className="row ">
                <div className="form-group col-4">
                  <label>Mã nhân viên:</label>
                  <input
                    name="employeeID"
                    type="text"
                    className="form-control"
                    value={this.state.employeeID}
                    onChange={this.handleChange}
                  />
                  <p></p>
                </div>
                <div className="form-group col-4 ">
                  <label>Loại yêu cầu:</label>

                  <select
                    name="requestType"
                    className="form-control"
                    onChange={this.handleChange}
                  >
                    <option value="Đi muộn">Đi muộn</option>
                    <option value="Về sớm">Về sớm</option>
                  </select>
                </div>
                <div className="form-group col-4">
                  <label>Lý do:</label>
                  <input
                    name="requestReason"
                    type="text"
                    className="form-control"
                    value={this.state.requestReason}
                    onChange={this.handleChange}
                  />
                </div>
              </div>
              <div className="row">
                <div className="col-4">
                  <DatePicker
                    disabledDate={this.disabledDate}
                    name="requestDate"
                    defaultValue={moment()}
                    onChange={this.handleDateValue}
                  />
                </div>
                <div className="col-4">
                  <TimePicker
                    name="requestTime"
                    format={format}
                    onChange={this.handleTime}
                  />
                </div>
                <div className="col-4 ">
                  <button
                    type="button"
                    onClick={() => {
                      this.handleCreateNewRequest(this.state);
                    }}
                    className="btn border border-1 mr-4 "
                  >
                    Tạo yêu cầu
                  </button>
                  <button type="button" className="btn border border-1">
                    Cập nhật
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    addNewRequest: (newRequest) => {
      const action = {
        type: ADD_NEW_REQUEST,
        newRequest,
      };
      dispatch(action);
    },
  };
};

export default connect(null, mapDispatchToProps)(RequestForm);
