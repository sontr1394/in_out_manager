import React, { Component } from "react";
import InOutRequestList from "./InOutRequestList";
import RequestForm from "./RequestForm";
import RequestList from "./RequestList";

export default class EmployeeRequest extends Component {
  render() {
    console.log("main form");
    return (
      <div className="container">
        <RequestForm />
        <RequestList />
        <InOutRequestList />
      </div>
    );
  }
}
