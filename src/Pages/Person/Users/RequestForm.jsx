import { DatePicker, TimePicker } from "antd";
import moment from "moment";
import React, { Component } from "react";
import { connect } from "react-redux";
import Swal from "sweetalert2";
import {
  ADD_NEW_REQUEST,
  UPDATE_REQUEST,
} from "../../../redux/constant/personconst";

class RequestForm extends Component {
  state = {
    request: {
      requestID: "",
      employeeID: "",
      requestType: "Đi muộn",
      requestReason: "",
      requestDate: moment().format("YYYY-MM-DD"),
      requestTime: moment().format("HH:MM"),
      requestStatus: "Mới",
    },
  };

  UNSAFE_componentWillReceiveProps(nextProps, state) {
    if (nextProps.requestEdit) {
      this.setState(
        {
          request: nextProps.requestEdit,
        },
        () => {
          console.log("state cho form:", this.state);
        }
      );
    }
  }

  handleChange = (e) => {
    let tagInput = e.target;
    let { name, value } = tagInput;
    console.log("name, value ", name, value);
    this.setState(
      {
        request: { ...this.state.request, [name]: value },
      },
      () => {
        console.log(this.state);
      }
    );
  };

  handleDateValue = (date) => {
    this.setState(
      {
        request: {
          ...this.state.request,
          requestDate: moment(date).format("YYYY-MM-DD"),
        },
      },
      () => {
        console.log(this.state);
      }
    );
  };

  disabledDate = (current) => {
    // Can not select days before today and today
    return current && current.valueOf() < Date.now();
  };

  handleTime = (newTime) => {
    var time = newTime.format("HH:mm");
    this.setState(
      {
        request: { ...this.state.request, requestTime: time },
      },
      () => {
        console.log(this.state);
      }
    );
  };

  checkValid = () => {
    for (let key in this.state.request) {
      if (key !== "requestID" && this.state.request[key] === "") {
        return false;
      }
    }
    return true;
  };

  handleCreateNewRequest = (newRequest) => {
    if (this.checkValid()) {
      this.props.addNewRequest(newRequest);
    } else {
      Swal.fire({
        icon: "error",
        title: "Lỗi...",
        text: "Bạn phải nhập đủ tất cả các trường!",
      });
      // message.error("Phải nhập giá trị cho tất cả các trường");
    }
  };

  handleUpdateRequest = (request) => {
    if (this.state.request.requestID !== "") {
      this.props.updateRequest(request);
    } else {
      Swal.fire({
        icon: "error",
        title: "Lỗi...",
        text: "Bạn phải chọn yêu cầu để sửa!",
      });
    }
  };

  render() {
    // console.log("request form");
    // console.log("request edit truyen tu reducer", this.props.requestEdit);
    console.log("state cua form ", this.state.request);
    const format = "HH:mm";

    return (
      <div className="container">
        <div className="card">
          <div className="card-header bg-yellow-200 text-black h4 text-center">
            Đăng ký yêu cầu
          </div>
          <div className="card-body">
            <form>
              <div className="row ">
                <div className="form-group col-4">
                  <label>Mã nhân viên:</label>
                  <input
                    name="employeeID"
                    type="text"
                    className="form-control"
                    value={this.state.request.employeeID}
                    onChange={this.handleChange}
                  />
                  <p></p>
                </div>
                <div className="form-group col-4 ">
                  <label>Loại yêu cầu:</label>

                  <select
                    name="requestType"
                    className="form-control"
                    onChange={this.handleChange}
                    value={this.state.request.requestType}
                  >
                    <option value="Đi muộn">Đi muộn</option>
                    <option value="Về sớm">Về sớm</option>
                  </select>
                </div>
                <div className="form-group col-4">
                  <label>Lý do:</label>
                  <input
                    name="requestReason"
                    type="text"
                    className="form-control"
                    value={this.state.request.requestReason}
                    onChange={this.handleChange}
                  />
                </div>
              </div>
              <div className="row">
                <div className="col-4">
                  <DatePicker
                    disabledDate={this.disabledDate}
                    name="requestDate"
                    onChange={this.handleDateValue}
                    value={moment(this.state.request.requestDate)}
                    // defaultValue={moment()}
                  />
                </div>
                <div className="col-4">
                  <TimePicker
                    name="requestTime"
                    format={format}
                    onChange={this.handleTime}
                    value={moment(this.state.request.requestTime, format)}
                  />
                </div>
                <div className="col-4 ">
                  <button
                    type="button"
                    onClick={() => {
                      this.handleCreateNewRequest(this.state.request);
                    }}
                    className="btn border border-1 mr-4 "
                  >
                    Tạo yêu cầu
                  </button>
                  <button
                    type="button"
                    onClick={() => {
                      this.handleUpdateRequest(this.state.request);
                    }}
                    className="btn border border-1"
                  >
                    Cập nhật
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }

  // componentDidUpdate(prevProps, prevState) {
  //   if (
  //     prevProps.requestEdit !== null &&
  //     prevProps.requestEdit.requestID !== this.state.request.requestID
  //   ) {
  //     this.state.request = this.props.requestEdit;
  //   }
  // }
}

const mapDispatchToProps = (dispatch) => {
  return {
    addNewRequest: (newRequest) => {
      const action = {
        type: ADD_NEW_REQUEST,
        newRequest,
      };
      dispatch(action);
    },
    updateRequest: (request) => {
      const action = {
        type: UPDATE_REQUEST,
        request,
      };
      dispatch(action);
    },
  };
};

const mapStateToProps = (state) => {
  return {
    requestEdit: { ...state.EmployeeRequestReducer.requestEdit },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(RequestForm);
