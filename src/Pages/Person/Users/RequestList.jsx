import React, { Component } from "react";
import { connect } from "react-redux";
import {
  DELETE_REQUEST,
  EDIT_REQUEST,
} from "../../../redux/constant/personconst";

class RequestList extends Component {
  requestList = this.props.requestList;

  handleDeleteRequest = (requestID) => {
    let requestList = this.props.requestList;
    let index = requestList.findIndex((request) => {
      return request.requestID === requestID;
    });
    if (index !== -1) {
      let request = requestList[index];
      this.props.deleteRequest(request);
    }

    console.log("index cua vi tri delete", index);
  };

  handleEditRequest = (requestID) => {
    let requestList = this.props.requestList;
    let index = requestList.findIndex((request) => {
      return request.requestID === requestID;
    });
    if (index !== -1) {
      let request = requestList[index];
      this.props.editRequest(request);
    }

    console.log("index cua vi tri edit", index);
  };

  renderRequest = () => {
    const requestList = this.props.requestList;
    // console.log("prop truyen tu reducer ", requestList);
    return requestList.map((request, index) => {
      return (
        <tr key={index}>
          <td>{request.employeeID}</td>
          <td>{request.requestType}</td>
          <td>{request.requestReason}</td>
          <td>{request.requestDate}</td>
          <td>{request.requestTime}</td>
          <td>{request.requestStatus}</td>
          <td>
            <div>
              <button
                onClick={() => {
                  this.handleDeleteRequest(request.requestID);
                }}
                type="button"
                className="btn "
              >
                <i className="fa fa-trash" />
              </button>
              <button
                onClick={() => {
                  this.handleEditRequest(request.requestID);
                }}
                type="button"
                className="btn "
              >
                <i className="fa fa-pen-square" />
              </button>
            </div>
          </td>
        </tr>
      );
    });
  };

  render() {
    console.log("list form");
    return (
      <div className="container py-5">
        <table className="table">
          <thead>
            <tr>
              <th>Mã nhân viên</th>
              <th>Loại yêu cầu</th>
              <th>Lý do</th>
              <th>Ngày xin</th>
              <th>Thời gian</th>
              <th>Trạng thái</th>
              <th>Cập nhật</th>
            </tr>
          </thead>
          <tbody>{this.renderRequest()}</tbody>
        </table>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    requestList: state.EmployeeRequestReducer.requestList,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    editRequest: (request) => {
      const action = {
        type: EDIT_REQUEST,
        request,
      };
      dispatch(action);
    },

    deleteRequest: (request) => {
      const action = {
        type: DELETE_REQUEST,
        request,
      };
      dispatch(action);
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(RequestList);
